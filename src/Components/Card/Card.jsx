import React from "react";
import "./Card.css"

const Card = ({src, country, population, region, capital}) => {



  return (
    
    <div className="card">

      <div className="img-div">
        <img src={src} alt="" />
      </div>

      <h3 className="margin">{country}</h3>

      <div className="margin">
        <p>Population : {population}</p>
        <p>Region : {region}</p>
        <p>Capital : {capital}</p>
      </div>

    </div>
  );
};

export default Card;
