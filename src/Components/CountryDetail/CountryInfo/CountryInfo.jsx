import React, {useContext} from 'react';
import "./CountryInfo.css";
import BorderCountries from '../BorderCountries/BorderCountries';
import ThemeContext from '../../themeContext/ThemeContext';

const CountryInfo = ({country, countries}) => {
  const darkmode = useContext(ThemeContext)

  return (
    <div className='country-info margin'>
        <h2 >{country.name.common}</h2>
        <div className={`flex-row ${darkmode ? "darkMode" : "gray" }}`}>
            <div>
                <p> <b >Native name : </b >{Object.values(country.name.nativeName)[0].common}</p>
                <p> <b >Population : </b >{country.population}</p>
                <p> <b >Region : </b >{country.region} </p>
                <p><b > Sub Region : </b > {country.subregion}</p>
                <p> <b >Capital : </b > {country.capital}</p>
            </div>
            <div>
                <p> <b >Top Level Domain :</b > {country.tld}</p>
                <p><b >Currencies : </b > {Object.values(country.currencies)[0].name}</p>
                <p> <b >Languages : </b >{Object.values(country.languages).join(', ')}</p>
            </div>
        </div>
        <BorderCountries country={country} countries={countries}/>
    </div>
  )
}

export default CountryInfo