import React, {useContext} from "react";
import { Link, useParams } from "react-router-dom";
import CountryImage from "./CountryImage/CountryImage";
import CountryInfo from "./CountryInfo/CountryInfo";
import "./CountryDetails.css";
import ThemeContext from "../themeContext/ThemeContext"


const CountryDetail = ({ countries }) => {
  const { id } = useParams();
  const {darkmode }= useContext(ThemeContext)

  const country = countries.find((country) => {
    return country.ccn3 === id;
  });
  console.log(id, country);

  return (
    <>
      {country ? (
        <div className={`country-detail ${darkmode ? "darkMode" : null}`}>
          <div>
           <Link to={'/'}><button className={`${darkmode ? "darkMode" : null}`}><i className="fa-solid fa-arrow-left"></i><span className="margin-left">Back</span></button></Link> 
          </div>
          <div className="flex-row">
            <CountryImage country={country}/>
            <CountryInfo country={country} countries={countries}/>
          </div>
        </div>
      ) : null}
    </>
  );
};

export default CountryDetail;
