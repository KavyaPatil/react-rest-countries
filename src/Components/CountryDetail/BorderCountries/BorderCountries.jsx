import React from "react";
import { Link } from "react-router-dom";
import './BorderCountries.css'

const BorderCountries = ({ country, countries }) => {

  const borderCountries = country.borders;
console.log(country.borders)
  const borderCountriesDetails = countries.filter((country) => {
    return borderCountries?.includes(country.cca3);
  });

  return (
    <div className="borders">
     <span className="bold mar"> Border Countries : </span>
      {borderCountriesDetails.length ? borderCountriesDetails.map((country, index) => {
        return (
          <Link to={`/countries/${country.ccn3}`} key={index + country.name.common}>
            <div key={index + country.name.common}>
              {country.name.common}
            </div>
          </Link>
        );
      }) : <div>No Border Countries..</div>
      }
    </div>
  );
};

export default BorderCountries;
