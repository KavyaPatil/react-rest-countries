import React from 'react';
import "./CountryImage.css"

const CountryImage = ({country}) => {
  return (
    <div className='country-img'><img src={country.flags.png} alt={country.name.common} /></div>
  )
}

export default CountryImage