import React, {useContext} from 'react';
import ThemeContext from '../../themeContext/ThemeContext';

const Search = ({searchHandler, value}) => {

  const {darkmode} = useContext(ThemeContext);

  return (
    <div className={darkmode ? "darkmode" : "search"}>
          <span><i className="fas fa-search search-icon" style={{color: darkmode ? "white" : null}}></i></span>
          <input
          className={darkmode ? "darkmode" : "search"}
            type="text"
            id="search"
            name="search"
            placeholder="Search for a Country...."
            value={value}
            onChange={searchHandler}
          />
        </div>
  )
}

export default Search