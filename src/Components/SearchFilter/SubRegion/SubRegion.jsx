import React, {useContext} from 'react';
import ThemeContext from '../../themeContext/ThemeContext';

const SubRegion = ({subRegionHandler,set}) => {

  const {darkmode} = useContext(ThemeContext);

  return (
    <label htmlFor="sub-region" onChange={subRegionHandler}>
          <select name="filter" id="filter"  className={darkmode ? "darkmode" : null} >
            <option value="">Sub Region</option>
            {[...set].map((subregion, index) => {
              return <option value={subregion} key={index + subregion}>{subregion}</option>
            })}
          </select>
        </label>
  )
}

export default SubRegion