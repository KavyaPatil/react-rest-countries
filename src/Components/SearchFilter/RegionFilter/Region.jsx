import React, {useContext} from 'react';
import ThemeContext from '../../themeContext/ThemeContext';

const Region = ({filterHandler}) => {

  const {darkmode} = useContext(ThemeContext);
  
  return (
    <label htmlFor="filter" onChange={filterHandler}>
    <select name="filter" id="filter"  className={darkmode ? "darkmode" : null} >
      <option value="">Filter By Region</option>
      <option value="Africa">Africa</option>
      <option value="America">America</option>
      <option value="Asia">Asia</option>
      <option value="Europe">Europe</option>
      <option value="Oceania">Oceania</option>
    </select>
  </label>
  )
}

export default Region