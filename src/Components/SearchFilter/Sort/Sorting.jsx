import React, {useContext} from 'react';
import ThemeContext from '../../themeContext/ThemeContext';

const Sorting = ({sortingHandler}) => {

  const {darkmode} = useContext(ThemeContext);

  return (
    <label htmlFor="filter" onChange={sortingHandler}>
    <select name="filter" id="filter"  className ={darkmode ? "darkmode" : null} >
      <option value="">Sort the countries</option>
      <option value="population-ascending">By population(ascending)</option>
      <option value="population-descending">By population(descending)</option>
      <option value="area-ascending">By Area(ascending)</option>
      <option value="area-descending">By Area(descending)</option>
    </select>
  </label>
  )
}

export default Sorting;