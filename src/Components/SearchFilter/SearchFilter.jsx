import React from "react";
import "./SearchFilter.css";
import SubRegion from "./SubRegion/SubRegion";
import Region from "./RegionFilter/Region";
import Search from "./Search/Search";
import Sorting from "./Sort/Sorting";

const SearchFilter = ({
  searchHandler,
  filterHandler,
  set,
  subRegionHandler,
  sortingHandler,
}) => {
  return (
    <>
      <div className="flex">
        <Search searchHandler={searchHandler}/>
        <div>
          <Region filterHandler={filterHandler} />
          <SubRegion
            set={set}
            subRegionHandler={subRegionHandler}
          />
        </div>
      </div>
      <div className="margin">
        <Sorting sortingHandler={sortingHandler} />
      </div>
    </>
  );
};

export default SearchFilter;
