import React, { useContext } from "react";
import "./Header.css";
import ThemeContext from "../themeContext/ThemeContext";

const Header = () => {
  const { darkmode, setDarkMode } = useContext(ThemeContext);

  return (
    <header className={darkmode ? "darkMode" : null}>
      <div className="flex">
        <h2>Where in the world?</h2>
        <button
          onClick={() => {
            setDarkMode(!darkmode);
          }}
          className={darkmode ? "darkMode" : null}
        >
          <span>
            <i className="fa-regular fa-moon"></i>
          </span>
          <span> Dark mode</span>
        </button>
      </div>
    </header>
  );
};

export default Header;
