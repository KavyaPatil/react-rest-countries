import React, { createContext, useState } from 'react';

const ThemeContext = createContext();


export function ThemeProvider({ children }) {

  const [darkmode, setDarkMode] = useState(false);

  return (
    <ThemeContext.Provider value={{ darkmode, setDarkMode }}>
      {children}
    </ThemeContext.Provider>
  );
}

export default ThemeContext;