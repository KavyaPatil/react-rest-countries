import React from "react";
import "./ErrorPage.css";


const ErrorPage = () => {
  return (
    <div className="error-container">
      {/* <p className="error-code">Error 500</p>
      <p className="error-message">Oops! Something went wrong.</p> */}
      <img className="error-image" src="https://internetdevels.com/sites/default/files/public/blog_preview/404_page_cover.jpg" alt="Error Image" />
      {/* <p>Please try again later.</p>
      <button className="error-retry-button" onclick="retryRequest()">
        Retry
      </button> */}
    </div>
  );
};

export default ErrorPage;
