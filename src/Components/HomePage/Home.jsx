import React, { useState } from "react";
import Header from "../Header/Header";
import "./Home.css";
import SearchFilter from "../SearchFilter/SearchFilter";
import Card from "../Card/Card";
import ErrorPage from "../ErrorPage/ErrorPage";
import Loader from "../Loader/Loader";
import { Link } from "react-router-dom";

function sortedCountries(data, selected) {
  let sortedData = [];

  if (selected === "population-ascending") {
    sortedData = [...data].sort((c1, c2) => {
      return c1.population - c2.population;
    });
  } else if (selected === "population-descending") {
    sortedData = [...data].sort((c1, c2) => {
      return c2.population - c1.population;
    });
  } else if (selected === "area-ascending") {
    sortedData = [...data].sort((c1, c2) => {
      return c1.area - c2.area;
    });
  } else if (selected === "area-descending") {
    sortedData = [...data].sort((c1, c2) => {
      return c2.area - c1.area;
    });
  } else {
    sortedData = [...data];
  }

  return sortedData;
}

const Home = ({ countries, isLoading, error }) => {
  const [subregionVal, setSubregionVal] = useState("");
  const [searchValue, setSearchValue] = useState("");
  const [region, setRegion] = useState("");
  const [sortBy, setSortingBy] = useState("");

  const searchHandler = (event) => {
    const search = event.target.value.toLowerCase();
    setSearchValue(search);
  };

  const filterHandler = (event) => {
    const filter = event.target.value.toLowerCase();
    setRegion(filter);
    setSubregionVal("");
  };

  const subregion = countries.reduce((newSet, country) => {
    if (country.region.toLowerCase().includes(region.toLowerCase())) {
      newSet.add(country.subregion);
    }
    return newSet;
  }, new Set());

  const subRegionHandler = (event) => {
    setSubregionVal(event.target.value);
  };

  const sortingHandler = (event) => {
    const selected = event.target.value;
    setSortingBy(selected);
  };

  const regionList = countries.filter((country) => {
    return country.region.toLowerCase().includes(region);
  });

  const subRegionList = regionList.filter((country) => {
    return country.subregion
      ?.toLowerCase()
      .includes(subregionVal.toLowerCase());
  });

  const searchedList = subRegionList.filter((country) => {
    return country.name.official
      .toLowerCase()
      .includes(searchValue.toLowerCase());
  });

  const sortedList = sortedCountries(searchedList, sortBy);

  if (isLoading) {
    return <Loader />;
  }

  if (error) {
    return <ErrorPage />;
  }

  return (
    <>
      <main className="container">
        <SearchFilter
          searchHandler={searchHandler}
          filterHandler={filterHandler}
          set={[...subregion]}
          subRegionHandler={subRegionHandler}
          sortingHandler={sortingHandler}
        />
        <div className="countries-container">
          {sortedList.length ? (
            sortedList.map((country) => {
              console.log(country.ccn3);
              return (
                <Link to={`countries/${country.ccn3}`}>
                  <Card
                    key={country.name.official}
                    src={country.flags.png}
                    population={country.population}
                    region={country.region}
                    country={country.name.official}
                    capital={country.capital}
                  />
                </Link>
              );
            })
          ) : (
            <h3>No Countries Found...</h3>
          )}
        </div>
      </main>
    </>
  );
};

export default Home;
