import { useState, useContext, useEffect } from "react";
import { Route, Routes } from "react-router-dom";
import "./App.css";
import Home from "./Components/HomePage/Home";
import ThemeContext from "./Components/themeContext/ThemeContext";
import CountryDetail from "./Components/CountryDetail/CountryDetail";
import Header from "./Components/Header/Header";
import ErrorPage from "./Components/ErrorPage/ErrorPage";

function App() {
  const [fetchedData, setfetchedData] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [error, setError] = useState("");

  const { darkmode } = useContext(ThemeContext);

  useEffect(() => {
    async function fetchCountries() {
      try {
        const response = await fetch("https://restcountries.com/v3.1/all");

        const countriesList = await response.json();

        setfetchedData(countriesList);
      } catch (err) {
        setError(err);
      }
      setIsLoading(false);
    }
    fetchCountries();
  }, []);

  return (
    <div className={`${darkmode ? "darkmode" : null}`}>
      <Header />
      <Routes>
        <Route
          path="/"
          element={
            <Home countries={fetchedData} isLoading={isLoading} error={error} />
          }
        />
        <Route
          path="countries/:id"
          element={<CountryDetail countries={fetchedData} />}
        />
        <Route path="*" element={<ErrorPage />} />
      </Routes>
    </div>
  );
}

export default App;
